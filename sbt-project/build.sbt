name := "scala-project"

version := "1.0.0"

scalaVersion := "2.12.8"

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-effect" % "1.3.1",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test"
)
